<?php

namespace Team1\Instagram\Setup;


use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $connect = $setup;
        $setup->startSetup();
        $tableName = $setup->getTable("sm_images");
        if ($setup->getConnection()->isTableExists($tableName) != true) {
            $table = $connect->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'img_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ]
                )
                ->addColumn(
                    'img_title',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'title Image'
                )
                ->addColumn(
                    'img_url',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Url Image'
                )
                ->addColumn(
                    'is_active',
                    Table::TYPE_SMALLINT,
                    255,
                    ['nullable' => false,'default'=>1],
                    'Active Status'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Image Create At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                    'Image Update At'
                )
                ->setOption('charset','utf8');
            $connect->getConnection()->createTable($table);
        }
        $connect->endSetup();
    }
}