<?php


namespace Team1\Instagram\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
class Social extends Template implements BlockInterface
{
    protected $_template = "widget/social.phtml";
}