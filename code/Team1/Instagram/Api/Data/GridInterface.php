<?php
namespace Team1\Instagram\Api\Data;

interface GridInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const IMG_ID = 'img_id';
    const TITLE = 'img_title';
    const IMAGE_URL = 'img_url';
    const IS_ACTIVE = 'is_active';
    const UPDATED_AT = 'updated_at';
    const CREATED_AT = 'created_at';

    /**
     * Get ImgId.
     *
     * @return int
     */
    public function getImgId();

    /**
     * Set ImgId.
     */
    public function setImgId($imgId);

    /**
     * Get Title.
     *
     * @return varchar
     */
    public function getTitle();

    /**
     * Set Title.
     */
    public function setTitle($title);

    /**
     * Get Content.
     *
     * @return varchar
     */


    /**
     * Get Url Image.
     *
     * @return varchar
     */
    public function getImgUrl();

    /**
     * Set Url Image.
     */
    public function setImgUrl($imgUrl);

    /**
     * Get IsActive.
     *
     * @return varchar
     */
    public function getIsActive();

    /**
     * Set StartingPrice.
     */
    public function setIsActive($isActive);

    /**
     * Get UpdateTime.
     *
     * @return varchar
     */
    public function getUpdateTime();

    /**
     * Set UpdateTime.
     */
    public function setUpdateTime($updateTime);

    /**
     * Get CreatedAt.
     *
     * @return varchar
     */
    public function getCreatedAt();

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt);
}