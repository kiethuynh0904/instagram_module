<?php
namespace Team1\Instagram\Model;

use Team1\Instagram\Api\Data\GridInterface;
class Grid extends \Magento\Framework\Model\AbstractModel implements GridInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'sm_images';

    /**
     * @var string
     */
    protected $_cacheTag = 'sm_images';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'sm_images';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Team1\Instagram\Model\ResourceModel\Grid');
    }
    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getImgId()
    {
        return $this->getData(self::IMG_ID);
    }
    /**
     * Set EntityId.
     */
    public function setImgId($imgId)
    {
        return $this->setData(self::IMG_ID, $imgId);
    }

    /**
     * Get Title.
     *
     * @return varchar
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Set Title.
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Get PublishDate.
     *
     * @return varchar
     */
    public function getImgUrl()
    {
        return $this->getData(self::IMAGE_URL);
    }

    /**
     * Set Image Url
     */
    public function setImgUrl($imgUrl)
    {
        return $this->setData(self::IMAGE_URL, $imgUrl);
    }

    /**
     * Get IsActive.
     *
     * @return varchar
     */
    public function getIsActive()
    {
        return $this->getData(self::IS_ACTIVE);
    }

    /**
     * Set IsActive.
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }

    /**
     * Get UpdateTime.
     *
     * @return varchar
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * Set UpdateTime.
     */
    public function setUpdateTime($updateAt)
    {
        return $this->setData(self::UPDATED_AT, $updateAt);
    }

    /**
     * Get CreatedAt.
     *
     * @return varchar
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get Content.
     *
     * @return varchar
     */
    public function getContent()
    {
        // TODO: Implement getContent() method.
    }

    /**
     * Set Content.
     */
    public function setContent($content)
    {
        // TODO: Implement setContent() method.
    }

    /**
     * Get Publish Date.
     *
     * @return varchar
     */
}