<?php
namespace Team1\Instagram\Model\ResourceModel\Grid;
use Team1\Instagram\Model\Grid;
use Team1\Instagram\Model\ResourceModel\Grid as ResourceModelGrid;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'img_id';
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init(Grid::class, ResourceModelGrid::class);
    }
}
